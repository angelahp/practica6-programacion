package programa;

import java.util.Scanner;
import clases.Pasteleria;

/**
 * Clase Programa.
 */

public class Programa {

	/** The scan. */
	static Scanner scan = new Scanner(System.in);
	
	/** Opcion del menu (switch). */
	static int opcion;
	
	/** Pasteleria. */
	static Pasteleria pasteleria;

	/**
	 * Metodo main
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		do {
			mostrarOpciones();
			opcion = scan.nextInt();
			ejecutarOpciones(opcion);

		} while (opcion != 0);

	}

	/**
	 * Mostrar opciones.
	 */
	public static void mostrarOpciones() {

		System.out.println("\n------  BIENVENID@ A LA PASTELERĶA HERNANDEZ  ------\n");
		System.out.println("1. Crear Instancia de Pasteleria.");
		System.out.println("2. Dar de Alta Empleados.");
		System.out.println("3. Buscas Empleado por Especialidad.");
		System.out.println("4. Eliminar Empleado.");
		System.out.println("\n5. Dar de Alta Pastelitos.");
		System.out.println("6. Buscar Pastelito.");
		System.out.println("7. Eliminar Pastelito.");
		System.out.println("8. Listar Pastelitos por Sabor.");
		System.out.println("9. Lista elementos de Pastelito y Empleado.");
		System.out.println("10. Asignar Empleado a Pastelito.");
		System.out.println("11. Control de Fechas.");
		System.out.println("12. Listar Empleados y Pastelitos.");
		System.out.println("0. Salir.");

		System.out.println("\nIntroduce una opcion: ");

	}

	/**
	 * Ejecutar opciones.
	 *
	 * @param opcion introducida por teclado
	 */
	public static void ejecutarOpciones(int opcion) {

		switch (opcion) {

		case 1: /* Crear Instancia */
			pasteleria = new Pasteleria();
			System.out.println("Instancia creada :)");
			break;

		case 2: /* Dar alta 3 Empleados + listar */

			scan.nextLine();

			System.out.println("\nIntroduce DNI del empleado numero 1: ");
			String dni = scan.nextLine();
			pasteleria.altaEmpleado(dni);

			System.out.println("\nIntroduce DNI del empleado numero 2: ");
			String dni2 = scan.nextLine();
			pasteleria.altaEmpleado(dni2);

			System.out.println("\nIntroduce DNI del empleado numero 3: ");
			String dni3 = scan.nextLine();
			pasteleria.altaEmpleado(dni3);

			pasteleria.listarEmpleado();

			break;

		case 3: /* Buscar Personal por Especialidad */
			scan.nextLine();

			System.out.println("\nIntroduce especialidad del empleado que quieres buscar: ");
			String especialidad = scan.nextLine();

			pasteleria.buscarEmpleadoEspecialidad(especialidad);

			break;

		case 4: /* Eliminar Personal + listar */
			scan.nextLine();

			System.out.println("\nIntroduce DNI del empleado que quieres eliminar: ");
			String eliminarDni = scan.nextLine();

			pasteleria.eliminarEmpleado(eliminarDni);
			pasteleria.listarEmpleado();
			break;

		case 5: /* Dar alta 3 Pastelitos + listar */
			scan.nextLine();

			System.out.println("\nIntroduce codigo del Pastelito(1/3): ");
			String codigo1 = scan.nextLine();
			pasteleria.altaPastelito(codigo1);

			System.out.println("\nIntroduce codigo del Pastelito(2/3): ");
			String codigo2 = scan.nextLine();
			pasteleria.altaPastelito(codigo2);

			System.out.println("\nIntroduce codigo del Pastelito(3/3): ");
			String codigo3 = scan.nextLine();
			pasteleria.altaPastelito(codigo3);

			pasteleria.listarPastelito();

			break;

		case 6: /* Buscar Pastelito */

			System.out.println("Introduce codigo del Pastelito: ");
			String buscarCodigo = scan.nextLine();
			System.out.println(pasteleria.buscarPastelito(buscarCodigo));

			break;

		case 7: /* Eliminar Pastelito + listar */

			scan.nextLine();

			System.out.println("\nIntroduce codigo del Pastelito que quieres eliminar: ");
			String eliminarCodigo = scan.nextLine();

			pasteleria.eliminarPastelito(eliminarCodigo);
			pasteleria.listarPastelito();

			break;

		case 8: /* Listar Pastelitos por Atributo (Sabor) */
			scan.nextLine();

			System.out.println("Introduce el sabor por el que quieres buscar: ");
			String sabor = scan.nextLine();

			pasteleria.buscarPastelitoSabor(sabor);

			break;

		case 9: 

			pasteleria.listarElementosPastelitoEmpleado();
			break;

		case 10:/* Asignar Empleado a Pastelito */
			scan.nextLine();

			System.out.println("Introduce dni del Empleado que quieres asignarle al Pastelito: ");
			String asignarDni = scan.nextLine();

			System.out.println("Introduce codigo del Pastelito al que le quieres asignar el anterior Empleado: ");
			String asignarCodigo = scan.nextLine();

			pasteleria.asignarEmpleado(asignarDni, asignarCodigo);
			pasteleria.buscarEmpleado(asignarDni);

			break;

		case 11: /* Controlar Fechas */
			scan.nextLine();

			pasteleria.controlFechas();
			break;

		case 12: /* Listar Empleados y Pastelitos*/

			pasteleria.listarEmpleado();
			System.out.println();
			pasteleria.listarPastelito();
			break;

		case 0: /* Salir del programa */
			System.exit(0);
			break;

		default: /* Opcion no contemplada */
			System.out.println("Opcion introducida no valida.");
			break;

		}
	}

}
