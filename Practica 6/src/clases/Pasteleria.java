package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Clase Pasteleria.
 */

public class Pasteleria {

	/** Scanner. */
	static Scanner scan = new Scanner(System.in);

	/** Lista pastelitos. */
	private ArrayList<Pastelito> listaPastelitos;
	
	/** Lista empleados. */
	private ArrayList<Empleado> listaEmpleados;

	/**
	 * Inicializacion de los ArrayLists
	 */
	
	public Pasteleria() {

		listaPastelitos = new ArrayList<Pastelito>();
		listaEmpleados = new ArrayList<Empleado>();

	}

	/* Empleado */

	/**
	 * Alta empleado.
	 *
	 * @param dni del empleado
	 */
	
	public void altaEmpleado(String dni) {

		if (!existeEmpleado(dni)) {

			Empleado nuevoEmpleado = new Empleado(dni);

			System.out.println("Introduce nombre del Empleado: ");
			String nombre = scan.nextLine();
			nuevoEmpleado.setNombre(nombre);

			System.out.println("Introduce especialidad del Empleado: ");
			String especialidad = scan.nextLine();
			nuevoEmpleado.setEspecialidad(especialidad);

			System.out.println("Introduce puesto del Empleado: ");
			String puesto = scan.nextLine();
			nuevoEmpleado.setPuesto(puesto);

			System.out.println("Introduce la fecha de contratacion del Empleado (aaaa/mm/dd)");
			String fecha = scan.nextLine();
			LocalDate fechaContratacion = LocalDate.parse(fecha);
			nuevoEmpleado.setFechaContratacion(fechaContratacion);

			System.out.println("Introduce edad del Empleado: ");
			int edad = scan.nextInt();
			nuevoEmpleado.setEdad(edad);

			System.out.println("Introduce sueldo (mensual) del Empleado: ");
			double sueldo = scan.nextDouble();
			nuevoEmpleado.setSueldo(sueldo);

			System.out.println("Introduce ventas del Empleado: ");
			int ventas = scan.nextInt();
			nuevoEmpleado.setVentas(ventas);

			listaEmpleados.add(nuevoEmpleado);
			
			scan.nextLine();
		} else {
			
			System.out.println("El DNI introducido ya ha sido almacenado.");
		}

	}

	/**
	 * Existe empleado.
	 *
	 * @param dni del empleado
	 * @return true si ya existe
	 * @return false si no existe
	 */
	
	public boolean existeEmpleado(String dni) {
		for (Empleado empleado : listaEmpleados) {
			if (empleado != null && empleado.getDni().equals(dni)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Listar empleado.
	 */
	
	public void listarEmpleado() {

		System.out.println("-> EMPLEADOS <-");
		for (Empleado empleado : listaEmpleados) {
			if (empleado != null) {
				System.out.println(empleado);
			}
		}
	}

	/**
	 * Buscar empleado.
	 *
	 * @param dni del empleado
	 * @return empleado
	 */
	
	public Empleado buscarEmpleado(String dni) {
		for (Empleado empleado : listaEmpleados) {
			if (empleado != null && empleado.getDni().equals(dni)) {
				return empleado;
			} 
		}
		return null;
	}
	
	/**
	 * Buscar empleado especialidad.
	 *
	 * @param especialidad del empleado
	 */
	
	public void buscarEmpleadoEspecialidad(String especialidad) {
		for (Empleado empleadillo : listaEmpleados) {
			if (empleadillo.getEspecialidad() != null && empleadillo.getEspecialidad().equals(especialidad)) {
				System.out.println(empleadillo);
			}
		}
	}

	/**
	 * Eliminar empleado.
	 *
	 * @param dni del empleado
	 */
	
	public void eliminarEmpleado(String dni) {

		Iterator<Empleado> iteradorEmpleado = listaEmpleados.iterator();

		while (iteradorEmpleado.hasNext()) {
			Empleado empleado = iteradorEmpleado.next();
			if (empleado.getDni().equals(dni)) {
				iteradorEmpleado.remove();
			}
		}
	}

	/* Pastelito */
	
	
	/**
	 * Alta Pastelito
	 *
	 * @param codigo del Pastelito
	 */

	public void altaPastelito(String codigo) {

		if (!existePastelito(codigo)) {

			Pastelito nuevoPastelito = new Pastelito(codigo);

			System.out.println("Introduce nombre del Pastelito: ");
			String nombre = scan.nextLine();
			nuevoPastelito.setNombre(nombre);

			System.out.println("Introduce sabor del Pastelito: ");
			String sabor = scan.nextLine();
			nuevoPastelito.setSabor(sabor);

			System.out.println("Introduce la fecha de elaboracion del Pastelito (aaaa/mm/dd): ");
			String fechaE = scan.nextLine();
			LocalDate fechaElaboracion = LocalDate.parse(fechaE);
			nuevoPastelito.setFechaElaboracion(fechaElaboracion);

			System.out.println("Introduce la fecha de caducidad del Pastelito (aaaa/mm/dd): ");
			String fechaC = scan.nextLine();
			LocalDate fechaCaducidad = LocalDate.parse(fechaC);
			nuevoPastelito.setFechaCaducidad(fechaCaducidad);

			System.out.println("Introduce calorias del Pastelito: ");
			double calorias = scan.nextDouble();
			nuevoPastelito.setCalorias(calorias);

			System.out.println("Introduce peso del Pastelito: ");
			double peso = scan.nextDouble();
			nuevoPastelito.setPeso(peso);
			
			listaPastelitos.add(nuevoPastelito);
			
			scan.nextLine();
		}

	}

	/**
	 * Existe pastelito.
	 *
	 * @param codigo del Pastelito
	 * @return true si ya existe
	 * @return false si no existe
	 */
	
	public boolean existePastelito(String codigo) {
		for (Pastelito pastelitos : listaPastelitos) {
			if (pastelitos != null && pastelitos.getCodigo().equals(codigo)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Eliminar pastelito.
	 *
	 * @param codigo del Pastelito
	 */
	
	public void eliminarPastelito(String codigo) {

		Iterator<Pastelito> iteradorPastelito = listaPastelitos.iterator();

		while (iteradorPastelito.hasNext()) {
			Pastelito pastelito = iteradorPastelito.next();
			if (pastelito.getCodigo().equals(codigo)) {
				iteradorPastelito.remove();
			}
		}
	}

	/**
	 * Buscar pastelito.
	 *
	 * @param codigo del Pastelito
	 * @return pastelito
	 */
	
	public Pastelito buscarPastelito(String codigo) {
		for (Pastelito pastelito : listaPastelitos) {
			if (pastelito != null && pastelito.getCodigo().equals(codigo)) {
				return pastelito;
			}
		}
		return null;
	}

	/**
	 * Listar pastelito.
	 */
	
	public void listarPastelito() {
		for (int i = 0; i < listaPastelitos.size(); i++) {
			if (listaPastelitos.get(i) != null) {
				System.out.println(listaPastelitos.get(i));
			}
		}
	}

	/**
	 * Buscar pastelito sabor.
	 *
	 * @param sabor del Pastelito
	 */
	
	public void buscarPastelitoSabor(String sabor) {
		for (Pastelito pastelito : listaPastelitos) {
			if (pastelito.getSabor() != null && pastelito.getSabor().equals(sabor)) {
				System.out.println(pastelito);
			}
		}
	}

	
	

	/**
	 * Listar elementos pastelito empleado.
	 */
	
	public void listarElementosPastelitoEmpleado() {

		for (int i = 0; i < listaPastelitos.size(); i++) {
			if (listaPastelitos.get(i) != null) {
				System.out.println(listaPastelitos.get(i).toString() + "\n");

			}
		}

	}

	/**
	 * Listar trabajos de responsable.
	 *
	 * @param dni del Pastelito
	 */
	
	public void listarTrabajosDeResponsable(String dni) {
		for (Pastelito pastelillo : listaPastelitos) {
			if (pastelillo.getEmpleado() != null && pastelillo.getEmpleado().getDni().equals(dni)) {
				System.out.println(pastelillo);
			}
		}
	}

	/**
	 * Asignar empleado.
	 *
	 * @param dni del Empleado
	 * @param codigo del Pastelito
	 */
	
	public void asignarEmpleado(String dni, String codigo) {
		if (buscarEmpleado(dni) != null && buscarPastelito(codigo) != null) {
			Empleado empleado = buscarEmpleado(dni);
			Pastelito pastelillo = buscarPastelito(codigo);
			pastelillo.setEmpleado(empleado);
		}
	}

	/* Metodo extra */

	/**
	 * Control fechas.
	 * 
	 * Comprueba que la fecha de Elaboracion no sea posterior que la de Caducidad
	 */
	
	public void controlFechas() {
		
		int fechaCorrecta= 0;

		Iterator<Pastelito> iteradorPastelito = listaPastelitos.iterator();

		while (iteradorPastelito.hasNext()) {
			Pastelito pastelito = iteradorPastelito.next();
			if (!pastelito.compararFechas()) {
				System.out.println("Se ha detectado un error en las fechas.");

				System.out.println("Introduce la nueva fecha de elaboracion.");
				String fechaE = scan.nextLine();
				LocalDate fechaElabo = LocalDate.parse(fechaE);
				pastelito.setFechaElaboracion(fechaElabo);

				System.out.println("Introduce la nueva fecha de caducidad.");
				String fechaC = scan.nextLine();
				LocalDate fechaCadu = LocalDate.parse(fechaC);
				pastelito.setFechaElaboracion(fechaCadu);
			} else {
				fechaCorrecta++;
			}
		}
		
		if (listaPastelitos.size() == fechaCorrecta) {
			System.out.println("- Fechas introducidas correctas :) -");
		}
	}
	
	

}
