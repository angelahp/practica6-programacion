package clases;

import java.time.LocalDate;

// TODO: Auto-generated Javadoc
/**
 * The Class Empleado.
 */
public class Empleado {

	//Atributos
	
	/** Nombre. */
	private String nombre;
	
	/** DNI. */
	private String dni;
	
	/** Especialidad. */
	private String especialidad;
	
	/** Puesto. */
	private String puesto;
	
	/** Edad. */
	private int edad;
	
	/** Sueldo. */
	private double sueldo;
	
	/** Ventas. */
	private int ventas;
	
	/** Fecha contratacion. */
	private LocalDate fechaContratacion;
	
	//Constructores
	
	/**
	 * Constructor inicializa Empleado.
	 */
	public Empleado() {
		this.nombre = "";
		this.edad = 0;
		this.dni = "";
		this.especialidad = "";
		this.sueldo = 0;
		this.puesto = "";
		this.ventas = 0;
	}

	
	/**
	 * Contructor Empleado
	 *
	 * @param dni del Empleado
	 */
	public Empleado(String dni) {
		this.dni = dni;
	}


	/**
	 * Instantiates a new empleado.
	 *
	 * @param nombre del Empleado
	 * @param edad del Empleado
	 * @param dni del Empleado
	 * @param especialidad del Empleado
	 * @param sueldo del Empleado
	 * @param puesto del Empleado
	 * @param ventas del Empleados
	 * @param fechaContratacion del Empleado
	 */
	public Empleado(String nombre, int edad, String dni, String especialidad, double sueldo, String puesto, int ventas,
			LocalDate fechaContratacion) {
		this.nombre = nombre;
		this.edad = edad;
		this.dni = dni;
		this.especialidad = especialidad;
		this.sueldo = sueldo;
		this.puesto = puesto;
		this.ventas = ventas;
		this.fechaContratacion = fechaContratacion;
	}
	
	
	//Setters y Getters
	
	/**
	 * Gets the nombre.
	 *
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}
	
	/**
	 * Sets the nombre.
	 *
	 * @param nombre the new nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * Gets the edad.
	 *
	 * @return the edad
	 */
	public int getEdad() {
		return edad;
	}
	
	/**
	 * Sets the edad.
	 *
	 * @param edad the new edad
	 */
	public void setEdad(int edad) {
		this.edad = edad;
	}
	
	
	/**
	 * Gets the dni.
	 *
	 * @return the dni
	 */
	public String getDni() {
		return dni;
	}
	
	/**
	 * Sets the dni.
	 *
	 * @param dni the new dni
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}
	
	/**
	 * Gets the especialidad.
	 *
	 * @return the especialidad
	 */
	public String getEspecialidad() {
		return especialidad;
	}
	
	/**
	 * Sets the especialidad.
	 *
	 * @param especialidad the new especialidad
	 */
	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}
	
	/**
	 * Gets the sueldo.
	 *
	 * @return the sueldo
	 */
	public double getSueldo() {
		return sueldo;
	}
	
	/**
	 * Sets the sueldo.
	 *
	 * @param sueldo the new sueldo
	 */
	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}
	
	/**
	 * Gets the puesto.
	 *
	 * @return the puesto
	 */
	public String getPuesto() {
		return puesto;
	}
	
	/**
	 * Sets the puesto.
	 *
	 * @param puesto the new puesto
	 */
	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}
	
	/**
	 * Gets the ventas.
	 *
	 * @return the ventas
	 */
	public int getVentas() {
		return ventas;
	}
	
	/**
	 * Sets the ventas.
	 *
	 * @param ventas the new ventas
	 */
	public void setVentas(int ventas) {
		this.ventas = ventas;
	}
	
	/**
	 * Gets the fecha contratacion.
	 *
	 * @return the fecha contratacion
	 */
	public LocalDate getFechaContratacion() {
		return fechaContratacion;
	}
	
	/**
	 * Sets the fecha contratacion.
	 *
	 * @param fechaContratacion the new fecha contratacion
	 */
	public void setFechaContratacion(LocalDate fechaContratacion) {
		this.fechaContratacion = fechaContratacion;
	}
	
	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "\nNombre: " + nombre + "\nEdad: " + edad + "\nDNI: " + dni + "\nEspecialidad: " + especialidad
				+ "\nSueldo: " + sueldo + " �" +"\nPuesto: " + puesto + "\nVentas: " + ventas + "\nFechaContratacion: "
				+ fechaContratacion + "\n";
	}	
}
