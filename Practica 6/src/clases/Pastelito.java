package clases;

import java.time.LocalDate;


/**
 * The Class Pastelito.
 */

public class Pastelito {

	
	/**
	 * @author Angela Hernandez
	 */
	
	
	
	// Atributos
	
	protected String nombre;
	
	/** Codigo. */
	private String codigo;
	
	/** Sabor. */
	private String sabor;
	
	/** Calorias. */
	private double calorias;
	
	/** Peso. */
	private double peso;
	
	/** Fecha elaboracion. */
	private LocalDate fechaElaboracion;
	
	/** Fecha caducidad. */
	private LocalDate fechaCaducidad;
	
	/** Empleado. */
	private Empleado empleado;

	// Constructores

	/**
	 * Constructor que inicializa Pastelito
	 */
	
	public Pastelito() {
		this.nombre = "";
		this.codigo = "";
		this.sabor = "";
		this.calorias = 0;
		this.peso = 0;
	}

	/**
	 * Constructor Pastelido
	 *
	 * @param codigo del Pastelito
	 */
	public Pastelito(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Atributos de un nuevo Pastelito.
	 *
	 * @param nombre del Pastelito
	 * @param codigo del Pastelito
	 * @param sabor del Pastelito
	 * @param calorias del Pastelito
	 * @param peso del Pastelito
	 * @param fechaElaboracion del Pastelito
	 * @param fechaCaducidad del Pastelito
	 */
	public Pastelito(String nombre, String codigo, String sabor, double calorias, double peso,
			LocalDate fechaElaboracion, LocalDate fechaCaducidad) {

		this.nombre = nombre;
		this.codigo = codigo;
		this.sabor = sabor;
		this.calorias = calorias;
		this.peso = peso;

	}

	// Setters y Getters

	/**
	 * Get nombre.
	 *
	 * @return nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Set nombre.
	 *
	 * @param nombre 
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Get codigo.
	 *
	 * @return codigo
	 */
	public String getCodigo() {
		return codigo;
	}

	/**
	 * Set codigo.
	 *
	 * @param codigo 
	 */
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Get sabor.
	 *
	 * @return sabor
	 */
	public String getSabor() {
		return sabor;
	}

	/**
	 * Set sabor.
	 *
	 * @param sabor
	 */
	public void setSabor(String sabor) {
		this.sabor = sabor;
	}

	/**
	 * Get calorias.
	 *
	 * @return calorias
	 */
	public double getCalorias() {
		return calorias;
	}

	/**
	 * Setcalorias.
	 *
	 * @param calorias
	 */
	public void setCalorias(double calorias) {
		this.calorias = calorias;
	}

	/**
	 * Get peso.
	 *
	 * @return peso
	 */
	public double getPeso() {
		return peso;
	}

	/**
	 * Set peso.
	 *
	 * @param peso
	 */
	public void setPeso(double peso) {
		this.peso = peso;
	}

	/**
	 * Get fecha elaboracion.
	 *
	 * @return fecha elaboracion
	 */
	public LocalDate getFechaElaboracion() {
		return fechaElaboracion;
	}

	/**
	 * Set fecha elaboracion.
	 *
	 * @param fechaElaboracion 
	 */
	public void setFechaElaboracion(LocalDate fechaElaboracion) {
		this.fechaElaboracion = fechaElaboracion;
	}

	/**
	 * Get fecha caducidad.
	 *
	 * @return fechacaducidad
	 */
	public LocalDate getFechaCaducidad() {
		return fechaCaducidad;
	}

	/**
	 * Set fecha caducidad.
	 *
	 * @param fechaCaducidad
	 */
	public void setFechaCaducidad(LocalDate fechaCaducidad) {
		this.fechaCaducidad = fechaCaducidad;
	}

	/**
	 * Get empleado.
	 *
	 * @return empleado
	 */
	public Empleado getEmpleado() {
		return empleado;
	}

	/**
	 * Set empleado.
	 *
	 * @param empleado
	 */
	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	
	/**
	 * To string.
	 *
	 * @return the string
	 */
	
	@Override
	public String toString() {
		return "\nNombre: " + nombre + "\nCodigo: " + codigo + "\nSabor: " + sabor + "\nCalorias: " + calorias
				+ "\nPeso: " + peso + "\nFechaElaboracion: " + fechaElaboracion + "\nFechaCaducidad: " + fechaCaducidad
				+ "\nEmpleado: " + empleado + "\n";

	}

	/**
	 * Comparar fechas
	 *
	 * @return true fechas correctas
	 * @return false fechas incorrectas
	 */
	public boolean compararFechas() {
		
		if(getFechaElaboracion().isAfter(getFechaCaducidad())){
			return false;
		}else {
			return true;
		}
	}
}
