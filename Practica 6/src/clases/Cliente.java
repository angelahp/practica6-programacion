package clases;

public class Cliente {

	private String nombre;
	private String dni;
	private String tipoCliente;
	private int visitas;
	
	
	
	public Cliente() {
		this.nombre = "";
		this.dni = "";
		this.tipoCliente = "";
		this.visitas = 0;
	}
	
	public Cliente(String dni) {
		
		this.dni = "";
		
	}
	
	public Cliente(String nombre, String dni, String tipoCliente, int visitas) {
		this.nombre = nombre;
		this.dni = dni;
		this.tipoCliente = tipoCliente;
		this.visitas = visitas;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	public int getVisitas() {
		return visitas;
	}

	public void setVisitas(int visitas) {
		this.visitas = visitas;
	}

	@Override
	public String toString() {
		return "\nNombre: " + nombre + "\nDNI: " + dni + "\nTipoCliente:" + tipoCliente + "\nVisitas:" + visitas;
	}
	
	
	
	
	
}
